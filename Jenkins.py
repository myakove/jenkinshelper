#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
import user
import argparse
import ConfigParser
from JenkinsHelper import JenkinsConnection, C, JenkinsJobsAndView, \
    JenkinsRunActions

CONF_FILE = user.home + "/.jenkinsCLI.conf"


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawTextHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    """
    Format argparse output
    """
    pass

PARSER = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                 prog="Jenkins actions",
                                 usage="%(prog)s [options]")

GROUP_REQUIRED = PARSER.add_argument_group("required arguments")
GROUP_OPTIONAL = PARSER.add_argument_group("optional arguments")

GROUP_OPTIONAL.add_argument("-ex", "--exclude", help="Exclude from rebuild "
                                                     "by failed reason",
                            default=None)
GROUP_REQUIRED.add_argument("-a", "--action",  help='''action to run on the
job.
valid actions are:

"enable", "disable", "print", "build", "info", "delete",
"is_queued", "is_running", "stop", "rebuild", "get_last",
"delete_from_queue", "get_ver_status", "compare",
"rebuild_by_ver"''', required=True)
GROUP_OPTIONAL.add_argument("-s", "--search",  help='''\
search for job.
send <search1,search2> to search for <search1> and <search2>
send <search1:search2> to search for <search1> or <search2>''')
GROUP_REQUIRED.add_argument("-v", "--view", help="View in Jenkins, "
                                                 "send Network/3 .3-git/rest "
                                                 "for nested view",
                            default="All", required=True)
GROUP_OPTIONAL.add_argument("-f", "--file", help='''\
conf file with server, user and password.
File should be locate at $HOME/.jenkinsCLI.conf
Example file:
################################
# [SETTING]                    #
# server=http://jenkins.server #
# username=username            #
# password=password            #
################################''', action="store_false")
GROUP_OPTIONAL.add_argument("-bv", "--build_version", help="build version to "
                                                           "look for, only "
                                                           "work with get_ver_"
                                                           "status and rebuild"
                                                           "_by_ver",
                            default=None)
GROUP_OPTIONAL.add_argument("-bc", "--build_compare", help="build version to "
                                                           "compare for, only "
                                                           "work with compare",
                            default=None)
GROUP_OPTIONAL.add_argument("-w", "--wiki", help="output in wiki format",
                            action="store_true")
GROUP_OPTIONAL.add_argument("-jk", "--job_key", help="Key to find the error "
                                                     "in the job, some common "
                                                     "word that exist in all "
                                                     "the jobs (For network "
                                                     "jobs is the word "
                                                     "'network')")
GROUP_OPTIONAL.add_argument("-js", "--job_status", help="Status to match for "
                                                        "rebuild job by "
                                                        "version ",
                            default=None)
GROUP_OPTIONAL.add_argument("-sm", "--summary", help="Display summary of "
                                                     "total jobs",
                            action="store_true")
GROUP_OPTIONAL.add_argument("-l", "--last", help="Get version status and "
                                                 "rebuild job only by"
                                                 "last build status",
                            action="store_true")
ARGS = PARSER.parse_args()


def validate_action(action):
    action_list = ["enable", "disable", "print", "build", "delete",
                   "is_queued", "is_running", "stop", "rebuild",
                   "get_last", "delete_from_queue", "get_ver_status",
                   "compare", "rebuild_by_ver", "kill"]

    if ARGS.action not in action_list:
        found_action = False
        for action in action_list:
            if ARGS.action in action:
                found_action = True
                print "Did you mean:", C["white"], action, C["clear"]
                break

        if not found_action:
            print "Action not supported", C["red"], ARGS.action, C[
                "clear"]
            print C["green"], "Supported actions are:", C[
                "clear"]
            print action_list
        return False

    if ARGS.action == "compare":
        if not (ARGS.build_version and ARGS.build_compare):
            print C["red"],\
                "build_version and build_compare is needed for compare",\
                C["clear"]
            PARSER.print_help()
            return False

    if ARGS.summary and action != "get_ver_status":
        print C["red"], "summary only work with get_ver_status"
        return False

    if ARGS.action == "get_ver_status":
        if not ARGS.build_version:
            print C["red"], \
                "build_version is needed for get_ver_status", \
                C["clear"]
            PARSER.print_help()
            return False

    if ARGS.action == "rebuild_by_ver":
        if ARGS.exclude and not ARGS.job_key:
            print C["red"], "job_key is needed for exclude", \
                C["clear"]
            PARSER.print_help()
            return False

    if ARGS.job_status and not ARGS.action == "rebuild_by_ver":
        print C["red"], "job_status only work with rebuild_by_ver", \
            C["clear"]
        PARSER.print_help()
        return False

    return True


def validate_input():
    if not os.path.isfile(CONF_FILE):
        print CONF_FILE, "is missing"
        return False

    config = ConfigParser.RawConfigParser()
    config.read(CONF_FILE)
    server = config.get("SETTING", "server")
    username = config.get("SETTING", "username")
    password = config.get("SETTING", "password")

    jenkins_obj = JenkinsConnection(server=server,
                                    username=username,
                                    password=password)

    jenkins_connection = jenkins_obj.jenkins_connection()

    jobs_and_view = JenkinsJobsAndView(
        jenkins_connection=jenkins_connection, view=ARGS.view)

    view_url = jobs_and_view.get_view()
    jobs_dict = jobs_and_view.get_jobs_dict()
    if not jobs_dict:
        return False

    actions = JenkinsRunActions(jobs_dict=jobs_dict,
                                jenkins_connection=jenkins_connection)

    actions.job_actions(action=ARGS.action, search=ARGS.search,
                        build_ver=ARGS.build_version,
                        build_compare=ARGS.build_compare,
                        exclude=ARGS.exclude, wiki=ARGS.wiki,
                        job_key=ARGS.job_key, status=ARGS.job_status,
                        summary=ARGS.summary, view_url=view_url,
                        last=ARGS.last)

if __name__ == '__main__':
    if validate_action(ARGS.action):
        validate_input()
