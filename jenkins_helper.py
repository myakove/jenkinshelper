#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Jenkins helper functions
"""

import re
import logging
from jenkinsapi.custom_exceptions import (
    JenkinsAPIException, NoBuildData, NoResults
)

C = {"black": "\033[0;30m",
     "dark_gray": "\033[1;30m",
     "blue": "\033[0;34m",
     "light_blue": "\033[1;34m",
     "green": "\033[0;32m",
     "light_green": "\033[1;32m",
     "cyan": "\033[0;36m",
     "light_cyan": "\033[1;36m",
     "red": "\033[0;31m",
     "light_red": "\033[1;31m",
     "purple": "\033[0;35m",
     "light_purple": "\033[1;35m",
     "brown": "\033[0;33m",
     "yellow": "\033[1;33m",
     "light_gray": "\033[0;37m",
     "white": "\033[1;37m",
     "clear": "\033[0m"}


class JenkinsHelper(object):
    """
    Get all jobs for view from jenkins
    """
    def __init__(self, server, username, password, view="All"):
        self.server = server
        self.username = username
        self.password = password
        self.view = view

        try:
            from jenkinsapi.jenkins import Jenkins
        except ImportError:
            logging.error(
                "jenkinsapi is not installed, please install it using: pip "
                "install jenkinsapi"
            )
            raise ImportError('jenkinsapi not installed')

        self.jenkins_connection = Jenkins(
            baseurl=self.server, username=self.username,
            password=self.password
        )

        jenkins_url = self.jenkins_connection.base_server_url()
        split_view = self.view.split("/")
        view_url_path = "/view/".join(split_view)
        self.view_url = "/view/".join([jenkins_url, view_url_path])
        try:
            view_by_url = self.jenkins_connection.get_view_by_url(
                self.view_url
            )
            self.jobs_dict = view_by_url.get_job_dict().keys()
        except JenkinsAPIException:
            print "%s not found on Jenkins server (404)" % self.view_url
            self.jobs_dict = None

    def actions(
        self, action, version=None, build_compare=None, search=None,
        status=None, summary=False, results=False
    ):
        """
        Run actions for job on Jenkins server
        :param action:
        :param version:
        :param build_compare:
        :param search:
        :param status:
        :param summary:
        :param print_job:
        :param last:
        :param results:
        :return:
        """
        output = ""
        status_dict = {
            "Success": 0, "Failed": 0, "Unstable": 0, "Disabled": 0, "N/A": 0
        }
        jobs = []
        for job in self.jobs_dict:
            if search:
                if "," in search:
                    res = True
                    search_list = search.split(",")
                    for val in search_list:
                        if val not in job:
                            res = False
                            break
                    if res:
                        job = self.jenkins_connection.get_job(job)
                        jobs.append(job)

                else:
                    search_list = search.split(":")
                    for val in search_list:
                        if val in job:
                            job = self.jenkins_connection.get_job(job)
                            jobs.append(job)

                        else:
                            continue

            else:
                job = self.jenkins_connection.get_job(job)
                jobs.append(job)

        for job in jobs:
                ver_status = self.run_actions(
                    job=job, action=action, version=version,
                    build_compare=build_compare, status=status, results=results
                )
                if summary:
                    status_dict[ver_status.strip()] += 1
                output += "{}\n".format(ver_status)

        if summary:
            total_jobs = sum(status_dict.values())
            space_len = len("Total jobs:")
            title = "Total jobs: {}{}{}".format(
                C["white"], total_jobs, C["clear"]
            )
            for val in status_dict.keys():
                space = space_len - len(val)
                color = C["white"]
                if val == "Disabled":
                    color = C["dark_gray"]
                if val == "Failed":
                    color = C["red"]
                if val == "Unstable":
                    color = C["yellow"]
                if val == "Success":
                    color = C["green"]

                return "{}\n{}{}{}{}{}".format(
                    title, color, val, space * " ", status_dict[val],
                    C["clear"]
                )

        return output

    def run_actions(
        self, job, action, version=None, build_compare=None, status=None,
        results=False
    ):
        """
        Run actions for job on Jenkins server
        :param job:
        :param action:
        :param version:
        :param build_compare:
        :param status:
        :param results:
        :return:
        """
        if action == "enable":
            return self.enable_job(job=job)

        if action == "disable":
            return self.disable_job(job=job)

        if action == "list":
            return self.print_job(job=job)

        if action == "build":
            return self.build_job(job=job)

        if action == "is_queued":
            return self.is_job_queued(job=job)

        if action == "is_running":
            return self.is_job_running(job=job)

        if action == "stop":
            return self.stop_job(job=job)

        if action == "delete":
            return self.delete_job(job=job)

        if action == "rebuild":
            return self.rebuild_job(job=job)

        if action == "get_last":
            return self.get_last_job_result(job=job, results=results)

        if action == "delete_from_queue":
            return self.delete_job_from_queue(job=job)

        if action == "get_ver_status":
            return self.get_best_status_by_version(
                job=job, version=version, results=results
            )
        if action == "compare":
            return self.compare_status_by_version(
                job=job, version1=version, version2=build_compare,
            )
        if action == "rebuild_by_ver":
            return self.rebuild_job_by_version_status(
                job=job, version=version, status=status
            )
        if action == "kill":
            return self.kill_job(job=job)

        if action == "duration":
            return self.get_job_duration(job)

    def print_job(self, job):
        """
        Print job name
        :param job: Job to print
        :type job: object
        :return: Job name
        :rtype: str
        """
        job_len = 60
        space = job_len - len(job.name)
        return job.name + space * " "

    def enable_job(self, job):
        """
        Enable job
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        job.enable()
        return self.generate_output(job, text="Enabled")

    def disable_job(self, job):
        """
        Disable job
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        job.disable()
        return self.generate_output(job, text="Disabled")

    def build_job(self, job):
        """
        Build job
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        queued = job.is_queued()
        running = job.is_running()
        if running:
            return self.generate_output(job, text="Already running")

        if queued:
            return self.generate_output(job, text="Already in queue")

        if self.is_job_disabled(job):
            return self.generate_output(job, text="job is disabled")

        try:
            self.jenkins_connection.build_job(job.name)
        except AttributeError:
            job.post_data(job.get_build_triggerurl()[0], "build")
        return self.generate_output(job, text="Building")

    def delete_job(self, job):
        """
        Delete job
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        self.jenkins_connection.delete_job(job.name)
        return self.generate_output(job, text="Deleted")

    def is_job_queued(self, job):
        """
        Check if job is in queue
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        out = "In queue" if job.is_queued() else "Not in queue"
        return self.generate_output(job, text=out)

    def is_job_running(self, job):
        """
        Check if job is running
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        running = job.is_running()
        out = "Yes" if running else "No"
        return self.generate_output(job, text=out)

    def stop_job(self, job):
        """
        Stop job
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        if job.is_queued():
            return self.generate_output(job, text="Is in queue")
        if not job.is_running():
            return self.generate_output(job, text="Is not running")
        running_build = job.get_last_build()
        running_build.stop()
        return self.generate_output(job, text="Stopped")

    def rebuild_job(self, job):
        """
        Rebuild job
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        if job.is_running():
            return self.generate_output(job, text="Already running")

        if job.is_queued():
            return self.generate_output(job, text="Already in queue")

        if self.is_job_disabled(job):
            return self.generate_output(job, text="job is disabled")

        last_build = job.get_last_completed_build()
        if last_build.is_good():
            return self.generate_output(
                job, text="Last build was successful, No need to rebuild"
            )
        try:
            self.jenkins_connection.build_job(job.name)
        except AttributeError:
            job.post_data(job.get_build_triggerurl()[0], "build")

        return self.generate_output(job, text="Rebuilding")

    def get_last_job_result(self, job, results=False):
        """
        Get last result for job
        :param job: Job
        :type job: object
        :param results: Get cases results
        :type results: bool
        :return: output
        :rtype: str
        """
        try:
            last_run = job.get_last_buildnumber()
        except NoBuildData:
            return self.generate_output(
                job, text="no results (job never run?)"
            )
        return self.get_build_summery(job=job, build=last_run, results=results)

    def delete_job_from_queue(self, job):
        """
        Delete job from queue
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        if job.is_queued():
            queue = self.jenkins_connection.get_queue()
            job_queue = queue.get_queue_items_for_job(job.name)
            if not queue.delete_item(job_queue[0]):
                return False

            return self.generate_output(job, text="Deleted from queue")
        else:
            return self.generate_output(job, text="Not in queue")

    def kill_job(self, job):
        """
        Kill running job
        :param job: Job
        :type job: object
        :return: output
        :rtype: str
        """
        if job.is_queued():
            return self.delete_job_from_queue(job)

        if not job.is_running():
            return self.generate_output(job, text="Is not running")

        running_build = job.get_last_build()
        running_build.stop()
        return self.generate_output(job, text="Stopped")

    def get_best_status_by_version(self, job, version, results=False):
        """
        Get the best status by version
        :param job: Job object
        :type job: object
        :param version: Version
        :type version: str
        :param results: Get cases results
        :type results: bool
        :return: Best status for version
        :rtype: str
        """
        status, build = self.get_version_status(job=job, version=version)
        if not status or not build:
            return self.generate_output(job=job, text="No status for {"
                                                      "}".format(version))
        return self.get_build_summery(job=job, build=build, results=results)

    def rebuild_job_by_version_status(self, job, version, status=None):
        """
        Rebuild job by version status
        :param job: Job
        :type job: object
        :param version: Version to rebuild
        :type version: str
        :param status: Rebuild for status
        :type status: str
        :return: output
        :rtype: str
        """
        version_status, build_num = self.get_version_status(
            job=job, version=version
        )
        if build_num is None:
            return False

        if version_status.lower() == "Success":
            return self.generate_output(
                job, text="Last build was successful, No need to rebuild"
            )

        if (version_status.lower() == "Unstable" or
                version_status.lower() == "Failed"):
            if job.is_running():
                return self.generate_output(job, text="Already running")

            if job.is_queued():
                return self.generate_output(job, text="Already in queue")

            if status:
                if version_status.lower() == status:
                    try:
                        self.jenkins_connection.build_job(job.name)
                    except AttributeError:
                        job.post_data(
                            job.get_build_triggerurl()[0], "build"
                        )
                    return self.generate_output(job, text="Rebuilding")

                else:
                    return self.generate_output(
                        job,
                        text="Not match status, No need to rebuild"
                    )
            else:
                try:
                    self.jenkins_connection.build_job(job.name)
                except AttributeError:
                    job.post_data(
                        job.get_build_triggerurl()[0], "build"
                    )

                return self.generate_output(job, text="Rebuilding")

    def compare_status_by_version(self, job, version1, version2):
        """
        Compare two version statuses
        :param job: Job
        :type job: object
        :param version1: Version 1
        :type version1: str
        :param version2: Version 2
        :type version2: str
        :return: output
        :rtype: str
        """
        version_status_1 = self.get_version_status(job, version1)[0]
        version_status_2 = self.get_version_status(job, version2)[0]
        if version_status_1 == "Success":
            version_status_1 += " "
            color = "green"
            v1_status = C[color] + version_status_1 + version1
        elif version_status_1 == "Unstable":
            color = "yellow"
            v1_status = C[color] + version_status_1 + version1
        elif version_status_1 == "Failed":
            version_status_1 += "  "
            color = "red"
            v1_status = C[color] + version_status_1 + version1
        elif version_status_1 == "Disabled":
            color = "dark_gray"
            v1_status = C[color] + version_status_1 + version1
        else:
            version_status_1 += "     "
            color = "white"
            v1_status = C[color] + version_status_1 + version1
        if version_status_2 == "Success":
            version_status_2 += " "
            color = "green"
            v2_status = C[color] + version_status_2 + version2
        elif version_status_2 == "Unstable":
            color = "yellow"
            v2_status = C[color] + version_status_2 + version2
        elif version_status_2 == "Failed":
            version_status_2 += "  "
            color = "red"
            v2_status = C[color] + version_status_2 + version2
        elif version_status_2 == "Disabled":
            color = "dark_gray"
            v2_status = C[color] + version_status_2 + version2
        else:
            version_status_2 += "     "
            color = "white"
            v2_status = C[color] + version_status_2 + version2

        return "{}{}{}{}{}{}{}".format(
            C["white"], self.print_job(job), C["clear"], v1_status, C["clear"],
            v2_status, C["clear"]
        )

    def get_job_info(self, job, build):
        """
        Get build version, status, test_results
        :param job: Job
        :type job: object
        :param build: Build number
        :type build: int
        :return: version, status, test_results
        :rtype: list
        """
        build_obj = job.get_build(int(build))
        status = build_obj.get_status()
        console = build_obj.get_console()
        version = self.get_build_version(job=job, build=build)
        try:
            build_result_obj = build_obj.get_resultset()
            test_results = build_result_obj.items()
        except NoResults:
            test_results = None
        return version, status, test_results

    def get_job_builds_status_list(self, job, version):
        """
        Get job builds (up to 10 last builds) status and version for each build
        :param job: Job
        :type job: object
        :param version: Version to get status for
        :type version: str
        :return: build_list, status_list
        :rtype: list
        """
        build_list = []
        status_list = []

        builds = job.get_build_dict()
        builds_list = builds.keys()
        builds_list.reverse()

        for i in builds_list:
            build = job.get_build(i)
            build_version = self.get_build_version(job=job, build=i)

            if build_version == version:
                build_list.append(i)
                status = build.get_status()
                status_list.append(status)

        return build_list, status_list

    def is_job_disabled(self, job):
        """
        Check if job is disabled
        :param job: Job
        :type job: object
        :return: True/False
        :rtype: bool
        """
        return not job.is_enabled()

    def get_version_status(self, job, version):
        """
        Get job status by version
        :param job: Job
        :type job: object
        :param version: Version to get status for
        :type version: str
        :return: status and build
        :rtype: str
        """
        result_list = []
        unstable = False
        res = ("N/A", "N/A")
        builds, statuses = (
            self.get_job_builds_status_list(job, version=version)
        )

        if not statuses or not builds:
            return False

        for x, y in zip(statuses, builds):
            result_list.append((x, y))

        for result in result_list:
            status = result[0]
            build = result[1]

            if result[0] == "SUCCESS":
                return result

            if result[0] == "UNSTABLE":
                if not unstable:
                    unstable = True
                    res = (status, build)

            if result[0] == "FAILURE" and not unstable:
                res = (status, build)

        return res

    def get_job_summery(self, job, build):
        """
        Get cases summery for job
        :param job: Job
        :type job: object
        :param build: Build number
        :type build: int
        :return: output
        :rtype: str
        """
        job_build = job.get_build(int(build))
        console = job_build.get_console()
        summery = re.findall(r'Run summary:.*', console)
        if summery:
            c0 = C["green"]
            c1 = C["red"]
            c2 = C["yellow"]
            c3 = C["light_red"]
            white = C["white"]

            s_list = summery[0].split(":")
            title = "".join([s_list[0], ":"])
            res = s_list[1].strip()
            res_list = res.split(",")
            idx = 0
            output = [title]
            for i in res_list:
                space = 11 - len(i)
                if int(re.findall(r'\d+', i)[0]) > 0:
                    color = "c{}".format(idx)
                    res_out = "{}{}{}".format(
                        eval(color),
                        "".join([i, " " * space]), white)
                    output.append(res_out)
                else:
                    output.append("".join([i, " " * space]))
                idx += 1
            return " ".join(output)
        else:
            return "N/A"

    def get_results(self, test_results):
        """
        Get cases results from build
        :param test_results: Results object
        :type test_results: object
        :return: lists of passwd, failed, skipped
        :rtype: list
        """
        failed = []
        passed = []
        skipped = []
        for i in test_results:
            case_name = i[0].split(";")[1].split(".", 1)[-1]
            case_obj = i[1]
            case_result = case_obj.status
            if case_result == ("PASSED" or "FIXED"):
                passed.append((case_name, case_result))
            if case_result == ("REGRESSION" or "FAILED"):
                failed.append((case_name, case_result))
            if case_result == "SKIPPED":
                skipped.append((case_name, case_result))

        return passed, failed, skipped

    def get_build_summery(self, job, build, results=False):
        """
        Get build summery (Version status and test results)
        :param job: Job object
        :type job: object
        :param build: Build number
        :type build: int
        :param results: True to get build results
        :type results: bool
        :return: Build summery
        :rtype: str
        """
        cases = ""
        build_version, build_status, test_results = self.get_job_info(
            job=job, build=build
        )
        summery = self.get_job_summery(job=job, build=build)
        duration = self.get_job_duration(job=job, builds=[build])

        if results:
            passed, failed, skipped = self.get_results(test_results)
            f_job = "".join(["\t{}:{}\n".format(i[0], i[1]) for i in failed])
            s_job = "".join(["\t{}:{}\n".format(i[0], i[1]) for i in skipped])
            out_failed = "" if not failed else "\n{}Failed:\n{}".format(
                C["red"], f_job,
            )
            out_skipped = "" if not skipped else "\n{}Skipped:\n{}".format(
                C["yellow"], s_job,
            )
            cases = "{}{}{}".format(
                out_failed,
                out_skipped,
                C["clear"]
            )

        return self.generate_output(
            job=job, status=build_status, version=build_version,
            summery=summery, cases=cases, duration=duration
        )

    def generate_output(
        self, job, status="", version="", summery="", cases="", text="",
        duration=""
    ):
        """
        Generate output
        :param job: Job object
        :type job: object
        :param status: Build status
        :type status: str
        :param version: Status version
        :type version: str
        :param summery: Build summery
        :type summery: str
        :param cases: Build cases results
        :type cases: str
        :param text: Text to add to output
        :type text: str
        :param duration: Job duration
        :type duration: str
        :return: Output
        :rtype: str
        """
        color = "white"
        space = 10 - len(status)

        if status.lower() == "success":
            color = "green"

        elif status.lower() == "unstable":
            color = "yellow"

        elif status.lower() == "failed":
            color = "red"

        elif status.lower() == "disabled":
            color = "dark_gray"

        return "{}{}{}{}{}{}{}{}{}{}{} {}{}{}{}{}".format(
            C["white"],
            self.print_job(job),
            text,
            C["clear"],
            C[color],
            "".join([status.capitalize(), " " * space]),
            C["clear"],
            C["white"],
            C["clear"],
            C["green"],
            version,
            C["white"],
            summery,
            duration,
            C["clear"],
            cases
        )

    def get_build_version(self, job, build):
        """
        Get job version (RHEV-M)
        :param job: Job object
        :type job: object
        :param build: Build number
        :type build: int
        :return: version
        :rtype: str
        """
        try:
            job_build = job.get_build(int(build))
            data = job_build.get_data(
                job_build.python_api_url(job_build.baseurl)
            )
            description = data["description"] if data["description"] else ""
            version = re.findall(r'\(.*\)', description)[0].strip('()')
        except IndexError:
            version = "N/A"
        return version

    def get_job_duration(self, job, builds=None):
        """
        Get job duration
        :param job: Job object
        :type job: object
        :param builds: Builds number to get durations for
        :type builds: list
        :return: duration
        :rtype: str
        """
        output = ""
        if builds is None:
            builds = [job.get_last_build().buildno]

        for i in builds:
            build_job = job.get_build(i)
            if job.is_running():
                continue

            if self.is_job_aborted(job, i):
                continue

            if self.is_job_failed(job, i):
                continue

            test_results = self.get_job_info(job, i)[2]
            skipped = self.get_results(test_results)[2]
            if len(test_results) == len(skipped):
                continue

            job_duration = build_job.get_duration()
            duration_seconds = job_duration.seconds
            t = duration_seconds / 60
            t0 = t / 60
            t1 = t % 60
            h = t0 if len(str(t0)) > 1 else "0{}".format(t0)
            m = t1 if len(str(t1)) > 1 else "0{}".format(t1)
            output += "Runtime #{} {}:{}\n".format(i, h, m)

        return output

    def is_job_aborted(self, job, build):
        """
        Check if job build is aborted
        :param job: Job object
        :type job: object
        :param build: Build number
        :type build: int
        :return: True/False
        :rtype: bool
        """
        build_job = job.get_build(build)
        return build_job.get_status() == "ABORTED"

    def is_job_failed(self, job, build):
        """
        Check if job build is Failed
        :param job: Job object
        :type job: object
        :param build: Build number
        :type build: int
        :return: True/False
        :rtype: bool
        """
        build_job = job.get_build(build)
        return build_job.get_status() == "FAILURE"

    def get_job_builds(self, job):
        return job.get_build_dict()

if __name__ == "__main__":
    jobj = JenkinsHelper(
        server="http://jenkins.qa.lab.tlv.redhat.com:8080",
        username="qe_automation", password="123456", view="Network/3.5-git")
    import bpdb
    bpdb.set_trace()
    # jobj.actions(action="info")
    # print jobj.actions(action="get_ver_status",
    #                    # search="3.5-git-network_one-host-datacenter_networks",
    #                    version="vt13.4")
    # print jobj.actions(action="disable",
    #                    search="3.5-git-network_one-host-sanity_unittest")
    # print jobj.actions(action="enable",
    #                    search="3.5-git-network_one-host-sanity_unittest")

    # print jobj.actions(action="duration")
    # jobj.get_job_builds()
