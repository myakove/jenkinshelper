#! /usr/bin/python
# -*- coding: utf-8 -*-

import re
import logging
import urllib2
import BeautifulSoup
from jenkinsapi.custom_exceptions import(
    JenkinsAPIException, NoBuildData, NoResults
)

C = {"black": "\033[0;30m",
     "dark_gray": "\033[1;30m",
     "blue": "\033[0;34m",
     "light_blue": "\033[1;34m",
     "green": "\033[0;32m",
     "light_green": "\033[1;32m",
     "cyan": "\033[0;36m",
     "light_cyan": "\033[1;36m",
     "red": "\033[0;31m",
     "light_red": "\033[1;31m",
     "purple": "\033[0;35m",
     "light_purple": "\033[1;35m",
     "brown": "\033[0;33m",
     "yellow": "\033[1;33m",
     "light_gray": "\033[0;37m",
     "white": "\033[1;37m",
     "clear": "\033[0m"}


class JenkinsHelperFunctions(object):
    """
    Functions to get some info about jobs
    """
    def __init__(self, view_url, job_name):
        self.view_url = view_url
        self.job_name = job_name

    def is_job_disabled(self):
        build_url = "/".join([self.view_url, "job", self.job_name])
        url = urllib2.urlopen(build_url)
        soup = BeautifulSoup.BeautifulSoup(url)
        if "This project is currently disabled" in str(soup):
            return True
        return False

    def get_version_status(self, version, last=False):
        build_url = "/".join([self.view_url, "job", self.job_name])
        url = urllib2.urlopen(build_url)
        soup = BeautifulSoup.BeautifulSoup(url)
        res = soup.findAll('td')
        best_status = "N/A"
        build_num = "N/A"
        unstable = False

        if "This project is currently disabled" in str(soup):
            best_status = "Disabled"
            return best_status, None

        status1 = ["".join(re.findall(r'tooltip=.*"', str(i))) for i in res]
        status2 = filter(None, status1)
        status3 = [i.strip('tooltip="') for i in status2]
        status_final = [i.split()[0] for i in status3]

        version1 = [
            "".join(re.findall(r'Build (.{}*)'.format(version), str(i))) for i
            in res
        ]
        version_final = filter(None, version1)

        full1 = [
            "".join(re.findall(self.job_name + '/\d+/console', str(i))) for
            i in res
        ]
        full2 = filter(None, full1)
        build_number_final = re.findall(
            r'\d+', "".join(["".join(re.findall(r'/\d+/', i)) for i in full2])
        )

        result_list = []
        for x, y, z in zip(status_final, build_number_final, version_final):
            result_list.append((x, y))

        for result in result_list:
            if last:
                return result

            if result[0] == "Success":
                return result

            if result[0] == "Unstable":
                if not unstable:
                    build_num = result[1]
                    best_status = result[0]
                    unstable = True

            if result[0] == "Failed" and not unstable:
                build_num = result[1]
                best_status = result[0]

        return best_status, build_num

    def get_build_number_and_status(self):
        build = "N/A"
        status = "N/A"
        last_build_url = "/".join(
            [self.view_url, "job", self.job_name, "lastBuild"]
        )
        url = urllib2.urlopen(last_build_url)
        soup = BeautifulSoup.BeautifulSoup(url)
        string = soup.body.find('div', attrs={'id': 'description'}).text

        build_url = last_build_url.rstrip("lastBuild")
        url2 = urllib2.urlopen(build_url)
        soup2 = BeautifulSoup.BeautifulSoup(url2)

        if "This project is currently disabled" in str(soup2):
            status = "Disabled"
            build = "N/A"
            return build, status

        if "Build" in str(string):
            build = re.findall(r'\d*\w*.\d*', string)[2].rstrip(')')

        res = soup.findAll('img')
        for a in res:
            a = str(a)
            if re.findall("tooltip", a):
                status = a[a.find("tooltip") + len('tooltip="'):].rstrip(
                    "\" />'")

        return build, status

    def get_fail_test(self, job_run_url, job_key=None):
        test_err = "N/A"
        url = urllib2.urlopen(job_run_url)
        soup = BeautifulSoup.BeautifulSoup(url)
        res = soup.findAll()
        res_tr = soup.findAll('tr')
        res_set = set()

        if "This project is currently disabled" in str(soup):
            return False

        if "testReport" in job_run_url:
            for i in res_tr:
                if "failures" in str(i):
                    for x in str(i).split("href"):
                        if job_key in x:
                            idx1 = str(x).find(">" + job_key)
                            idx2 = str(x)[idx1:].find(";")
                            idx3 = str(x)[idx1+idx2:].find(".")
                            idx4 = str(x)[idx1+idx2+idx3:].find("</a>")
                            start_idx = idx1+idx2+idx3+1
                            end_idx = idx1+idx2+idx3+idx4
                            fail = str(x)[start_idx:end_idx]
                            if "</span>" not in fail:
                                res_set.add(fail)

            res_list = list(res_set)
            res_list.remove("")
            if res_list:
                test_err = ", ".join(res_list)

            else:
                if "All Failed Tests" in str(res[0]):
                    idx1 = str(res[0]).find("All Failed Tests")
                    idx2 = str(res[0])[idx1:].find('id="test-')
                    idx3 = str(res[0])[idx1+idx2:].find('-showlink"')
                    start_idx = idx1+idx2+9
                    end_idx = idx1+idx2+idx3
                    test_err = str(res[0])[start_idx:end_idx]

        if "consoleText" in job_run_url:
            idx1 = str(res).find("ERROR:")
            idx2 = str(res)[idx1:].find("\n")
            idx3 = idx1 + idx2
            idx4 = str(res).find("CommandExecutionError:")
            idx5 = str(res)[idx4:].find("\n")
            idx6 = idx4 + idx5
            test_err = str(res)[idx1:idx3] + ", " + str(res)[idx4:idx6]

        return test_err

    def get_job_result(self, job_run_url):
        try:
            url = urllib2.urlopen(job_run_url)
        except urllib2.HTTPError:
            return False, None
        soup = BeautifulSoup.BeautifulSoup(url)
        res = soup.findAll()
        if res:
            fail_idx1 = str(res[0]).find("failures")
            if fail_idx1 == -1:
                return 0, 0
            fail_idx2 = fail_idx1 - 3
            fail_num = str(res)[fail_idx2:fail_idx1]
            tests_idx1 = str(res[0]).find("tests")
            tests_idx2 = tests_idx1 - 3
            tests_num = str(res)[tests_idx2:tests_idx1]
            return int(tests_num), int(fail_num)
        return False, None

    def get_job_summery(self, job_run_url):
        job_run_url = "/".join(
            [job_run_url.strip("testReport/"), "consoleText"]
        )
        try:
            url = urllib2.urlopen(job_run_url)
        except urllib2.HTTPError:
            return "N/A"

        res = url.read()
        summery = re.findall(r'Run summary:.*', res)
        if summery:
            c0 = C["green"]
            c1 = C["red"]
            c2 = C["yellow"]
            c3 = C["light_red"]
            white = C["white"]

            s_list = summery[0].split(":")
            title = "".join([s_list[0], ":"])
            res = s_list[1].strip()
            res_list = res.split(",")
            idx = 0
            output = [title]
            for i in res_list:
                if int(re.findall(r'\d+', i)[0]) > 0:
                    color = "c{}".format(idx)
                    res_out = "{}{}{}".format(eval(color), i, white)
                    output.append(res_out)
                else:
                    output.append(i)
                idx += 1
            return " ".join(output)
        else:
            return "N/A"

class JenkinsConnection(object):
    """
    Description: Create connection to Jenkins server
    server - Jenkins server
    username - Jenkins server username
    password - Jenkins server password
    return jenkins connection object
    """
    def __init__(self, server, username, password):
        self.server = server
        self.username = username
        self.password = password

    def jenkins_connection(self):
        try:
            from jenkinsapi.jenkins import Jenkins
        except ImportError:
            logging.error(
                "jenkinsapi is not installed, please install it using: pip "
                "install jenkinsapi"
            )
            raise ImportError('jenkinsapi not installed')

        return Jenkins(
            baseurl=self.server, username=self.username,
            password=self.password
        )


class JenkinsJobsAndView(object):
    """
    Create Jenkins view object and get all jobs under the view
    view - Jenkins server view
    jenkins_connection - Jenkins connection object
    return Jenkins jobs Dictionary for the view
    """
    def __init__(self, jenkins_connection, view="All"):
        self.view = view
        self.jenkins_connection = jenkins_connection
        self.view_url = None

    def get_view(self):
        jenkins_url = self.jenkins_connection.base_server_url()
        split_view = self.view.split("/")
        view_url_path = "/view/".join(split_view)
        view_url = "/view/".join([jenkins_url, view_url_path])

        return view_url

    def get_jobs_dict(self):
        view_url = self.get_view()
        try:
            view_by_url = self.jenkins_connection.get_view_by_url(view_url)
        except JenkinsAPIException:
            print "%s not found on Jenkins server (404)" % view_url
            return False
        jobs_dict = view_by_url.get_job_dict().keys()
        return jobs_dict


class JenkinsRunActions(object):
    """
    Run actions on Jenkins server
    job_dict - Dictionary of jenkins jobs
    jenkins_connection - Jenkins connection object
    """
    def __init__(self, jobs_dict, jenkins_connection):
        self.jobs_dict = jobs_dict
        self.jenkins_connection = jenkins_connection
        self.helper_functions = None
        self.job = None

    def job_actions(
        self, action, build_ver=None, build_compare=None, search=None,
        exclude=None, wiki=None, job_key=None, status=None, summary=False,
        view_url=None, print_job=False, last=False
    ):
        pbar = None
        # try:
        #     from progressbar import ETA, Percentage, ProgressBar
        #     widgets = ['Progress: ', Percentage(), ' ', ' ', ETA()]
        #     pbar = ProgressBar(widgets=widgets)
        # except ImportError:
        #     print "For progress bar run: pip install progressbar"

        output = ""
        status_dict = {
            "Success": 0, "Failed": 0, "Unstable": 0, "Disabled": 0, "N/A": 0
        }
        jobs = pbar(self.jobs_dict) if pbar is not None else self.jobs_dict
        for job in jobs:
            job = self.jenkins_connection.get_job(job)

            if search:
                if "," in search:
                    res = True
                    search_list = search.split(",")
                    for val in search_list:
                        if val not in job.name:
                            res = False
                            break
                    if res:
                        self.job = job
                        job_name = self.job.name
                        self.helper_functions = JenkinsHelperFunctions(
                            view_url=view_url, job_name=job_name
                        )
                        ver_status = self.run_actions(
                            action, build_ver, build_compare,
                            exclude=exclude, wiki=wiki, job_key=job_key,
                            status=status, summary=summary, view_url=view_url,
                            print_job=print_job, last=last
                        )
                        if summary:
                            status_dict[ver_status.strip()] += 1
                        output += "{}\n".format(ver_status)

                else:
                    search_list = search.split(":")
                    for val in search_list:
                        if val in job.name:
                            self.job = job
                            job_name = self.job.name
                            self.helper_functions = JenkinsHelperFunctions(
                                view_url=view_url, job_name=job_name
                            )
                            ver_status = self.run_actions(
                                action, build_ver, build_compare,
                                exclude=exclude, wiki=wiki, job_key=job_key,
                                status=status, summary=summary,
                                view_url=view_url, print_job=print_job,
                                last=last
                            )
                            if summary:
                                status_dict[ver_status.strip()] += 1
                            output += "{}\n".format(ver_status)

                        else:
                            continue

            else:
                self.job = job
                job_name = self.job.name
                self.helper_functions = JenkinsHelperFunctions(
                    view_url=view_url, job_name=job_name
                )
                ver_status = self.run_actions(
                    action, build_ver, build_compare, exclude=exclude,
                    wiki=wiki, job_key=job_key, status=status, summary=summary,
                    view_url=view_url, print_job=print_job, last=last)
                if summary:
                    status_dict[ver_status.strip()] += 1
                output += "{}\n".format(ver_status)

        if summary:
            total_jobs = sum(status_dict.values())
            space_len = len("Total jobs:")
            print "Total jobs: ", C["white"], total_jobs, \
                C["clear"]
            for val in status_dict.keys():
                space = space_len - len(val)
                color = C["white"]
                if val == "Disabled":
                    color = C["dark_gray"]
                if val == "Failed":
                    color = C["red"]
                if val == "Unstable":
                    color = C["yellow"]
                if val == "Success":
                    color = C["green"]

                return "{}{}{}{}{}".format(
                    color, val, space*" ", status_dict[val], C["clear"]
                )

        return output

    def run_actions(self, action, build_ver=None, build_compare=None,
                    exclude=None, wiki=None, job_key=None, status=None,
                    summary=False, view_url=None, print_job=None, last=False):
        jenkins_action_functions = JenkinsActionFunctions(
            self.jenkins_connection, self.job
        )

        if action == "enable":
            return jenkins_action_functions.enable_job()

        if action == "disable":
            return jenkins_action_functions.disable_job()

        if action == "print":
            return jenkins_action_functions.print_job(print_job=print_job)

        if action == "build":
            return jenkins_action_functions.build_job(view_url=view_url)

        if action == "is_queued":
            return jenkins_action_functions.is_job_queued()

        if action == "is_running":
            return jenkins_action_functions.is_job_running()

        if action == "stop":
            return jenkins_action_functions.stop_job()

        if action == "delete":
            return jenkins_action_functions.delete_job()

        if action == "rebuild":
            return jenkins_action_functions.rebuild_job(view_url=view_url)

        if action == "get_last":
            return jenkins_action_functions.get_last_job_result(
                job_key=job_key, view_url=view_url
            )
        if action == "delete_from_queue":
            return jenkins_action_functions.delete_job_from_queue()

        if action == "get_ver_status":
            ver_status = jenkins_action_functions.get_job_status_by_version(
                build_ver=build_ver, wiki=wiki, view_url=view_url,
                job_key=job_key, summary=summary, last=last)

            if summary:
                if ver_status:
                    return ver_status
            else:
                return ver_status

        if action == "compare":
            return jenkins_action_functions.compare_status_by_version(
                build_ver=build_ver, build_compare=build_compare,
                view_url=view_url
            )
        if action == "rebuild_by_ver":
            return jenkins_action_functions.rebuild_job_by_version_status(
                build_ver=build_ver, exclude=exclude, job_key=job_key,
                status=status, view_url=view_url
            )
        if action == "kill":
            return jenkins_action_functions.kill_job()


class JenkinsActionFunctions(object):
    """
    Functions for actions on Jenkins job
    """
    def __init__(self, jenkins_connection, job):
        self.jenkins_connection = jenkins_connection
        self.job = job

    def print_job(self, print_job=False):
        job_len = 60
        space = job_len - len(self.job.name)
        if print_job:
            print "{}{}".format(
                C["white"] + self.job.name + space * " ", C["clear"]
            )
        return self.job.name + space * " "

    def enable_job(self):
        self.job.enable()
        out = self.print_job()
        return"{}{}{}enabled".format(C["white"], out, C["clear"])

    def disable_job(self):
        self.job.disable()
        out = self.print_job()
        return "{}{}{}disabled".format(C["white"], out, C["clear"])

    def build_job(self, view_url):
        jenkins_helper_functions = JenkinsHelperFunctions(
            job_name=self.job.name, view_url=view_url
        )
        queued = self.job.is_queued()
        running = self.job.is_running()
        out = self.print_job()
        if running:
            return "{}{}{}{}Already running{}".format(
                C["white"], out, C["clear"], C["green"], C["clear"]
            )
        if queued:
            return "{}{}{}{}already in queue{}".format(
                C["white"], out, C["clear"], C["green"], C["clear"]
            )
        if jenkins_helper_functions.is_job_disabled():
            return "{}{}{}{}job is disabled{}".format(
                C["white"], self.print_job(), C["clear"], C["dark_gray"],
                C["clear"]
            )
        try:
            self.jenkins_connection.build_job(self.job.name)
        except AttributeError:
            self.job.post_data(self.job.get_build_triggerurl()[0], "build")
        return "{}{}  building{}".format(C["white"], out, C["clear"])

    def delete_job(self):
        self.jenkins_connection.delete_job(self.job.name)
        return C["white"], self.print_job(), C["clear"], "Deleted"

    def is_job_queued(self):
        if self.job.is_queued():
            return "{}{}{}{}in queue{}".format(
                C["white"], self.print_job(), C["clear"], C["green"],
                C["clear"]
            )
        else:
            return "{}{}{}{}not in queue{}".format(
                C["white"], self.print_job(), C["clear"], C["green"],
                C["clear"]
            )

    def is_job_running(self):
        running = self.job.is_running()
        run = "Yes" if running else "No"
        return "{}{}{}running: {}{}{}".format(
            C["white"], self.print_job(), C["clear"], C["green"], run,
            C["clear"]
        )

    def stop_job(self):
        if self.job.is_queued():
            return "{}{}{}is in queue".format(
                C["white"], self.print_job(), C["clear"]
            )
        if not self.job.is_running():
            return "{}{}{}is not running".format(
                C["white"], self.print_job(), C["clear"]
            )
        running_build = self.job.get_last_build()
        running_build.stop()
        return "{}{}{}stopped".format(
            C["white"], self.print_job(), C["clear"]
        )

    def rebuild_job(self, view_url):
        jenkins_helper_functions = JenkinsHelperFunctions(
            job_name=self.job.name, view_url=view_url
        )
        if self.job.is_running():
            return "{}{}{}{}already running{}".format(
                C["white"], self.print_job(), C["clear"],C["green"], C["clear"]
            )
        if self.job.is_queued():
            return "{}{}{}{}already in queue{}".format(
                C["white"], self.print_job(), C["clear"], C["green"],
                C["clear"]
            )
        if jenkins_helper_functions.is_job_disabled():
            return "{}{}{}{}job is disabled{}".format(
                C["white"], self.print_job(), C["clear"], C["dark_gray"],
                C["clear"]
            )
        last_build = self.job.get_last_completed_build()
        if last_build.is_good():
            return (
                "{}{}{}{}Last build was successful, No need to rebuild{}".
                format(
                    C["white"], self.print_job(), C["clear"], C["white"],
                    C["clear"]
                )
            )
        try:
            self.jenkins_connection.build_job(self.job.name)
        except AttributeError:
            self.job.post_data(self.job.get_build_triggerurl()[0], "build")

        return "{}{}{}{}rebuilnding{}".format(
            C["white"], self.print_job(), C["clear"], C["green"], C["clear"]
        )

    def get_last_job_result(self, view_url, job_key=None):
        jenkins_helper_functions = JenkinsHelperFunctions(
            job_name=self.job.name, view_url=view_url
        )
        try:
            self.job.get_last_buildnumber()
        except NoBuildData:
            return "{}{}no results (job never run?){}".format(
                C["white"], self.print_job(), C["clear"]
            )
        err = "N/A"
        color = "white"
        build_version, build_status = (
            jenkins_helper_functions.get_build_number_and_status()
        )

        job_run_url = "{}/job{}/lastBuild/testReport/".format(
            view_url, self.job.name
        )
        try:
            urllib2.urlopen(job_run_url)
        except urllib2.HTTPError:
            job_run_url = "{}/job/{}/lastBuild/consoleText/".format(
                view_url, self.job.name
            )
        if build_status == "Success":
            build_status += "       "
            build_version += "   "
            color = "green"
        elif build_status == "Unstable":
            if job_key:
                err = jenkins_helper_functions.get_fail_test(
                    job_run_url, job_key
                )
            build_status += "      "
            build_version += "   "
            color = "yellow"
        elif build_status == "Failed":
            if job_key:
                err = jenkins_helper_functions.get_fail_test(
                    job_run_url, job_key
                )
            build_status += "        "
            build_version += "   "
            color = "red"
        elif build_status == "Disabled":
            build_status += "      "
            build_version += "   "
            color = "dark_gray"
        else:
            build_status += "   "
            build_version += "    "

        return "{}{}{}{}{}{}{}{}{}{}{}{}{}reason{}{}{}".format(
            C["white"], self.print_job(), C["clear"], C[color],
            build_status, C["clear"], C["white"], "Build", C["clear"],
            C["green"], build_version, C["clear"], C["white"], C[color],
            err, C["clear"]
        )

    def delete_job_from_queue(self):
        if self.job.is_queued():
            queue = self.jenkins_connection.get_queue()
            job_queue = queue.get_queue_items_for_job(self.job.name)
            if not queue.delete_item(job_queue[0]):
                return False

            return "{}{}{}{}deleted from queue{}".format(
                C["white"], self.print_job(), C["clear"], C["green"],
                C["clear"]
            )
        else:
            return "{}{}{}{}not in queue{}".format(
                C["white"], self.print_job(), C["clear"], C["green"],
                C["clear"]
            )

    def kill_job(self):
        if self.job.is_queued():
            self.delete_job_from_queue()
            return "{}{}{}removed from queue{}".format(
                C["white"], self.print_job(), C["clear"]
            )
        if not self.job.is_running():
            return "{}{}{}is not running".format(
                C["white"], self.print_job(), C["clear"]
            )
        running_build = self.job.get_last_build()
        running_build.stop()
        return "{}{}{}stopped".format(
            C["white"], self.print_job(), C["clear"]
        )

    def rebuild_job_by_version_status(
        self, build_ver, view_url, exclude=None, job_key=None, status=None
    ):
        jenkins_helper_functions = JenkinsHelperFunctions(
            job_name=self.job.name, view_url=view_url
        )
        version_status, build_num = (
            jenkins_helper_functions.get_version_status(build_ver)
        )
        color = ""
        if version_status == "Success":
            color = "green"
        elif version_status == "Unstable":
            color = "yellow"
        elif version_status == "Failed":
            color = "red"

        if build_num is None:
            return False

        job_run_url = "{}/job/{}/{}/testReport/".format(
            view_url. self.job.name, str(build_num.strip("\n"))
        )
        try:
            urllib2.urlopen(job_run_url)
        except urllib2.HTTPError:
            job_run_url = "{}/job/{}/{}/consoleText/".format(
                view_url, self.job.name, str(build_num.strip("\n"))
            )
        if version_status == "Success":
            return "{}{}{}{}{}:{}{}".format(
                C["white"], self.print_job(), C["clear"], C["green"],
                build_ver, version_status, C["clear"]
            )
        if version_status == "Unstable" or version_status == "Failed":
            if exclude:
                err = jenkins_helper_functions.get_fail_test(
                    job_run_url, job_key
                )
                if exclude in err:
                    return "{}{}{}{}excluded{}{}reason{}{}{}".format(
                        C["white"], self.print_job(), C["clear"],
                        C["yellow"], C["clear"], C["white"], C["yellow"],
                        err, C["clear"]
                    )
            if self.job.is_running():
                return "{}{}{}{}already running{}".format(
                    C["white"], self.print_job(), C["clear"], C["yellow"],
                    C["clear"]
                )

            if self.job.is_queued():
                return "{}{}{}{}already in queue{}".format(
                    C["white"], self.print_job(), C["clear"], C["yellow"],
                    C["clear"]
                )
            if status:
                if version_status == status:
                    try:
                        self.jenkins_connection.build_job(self.job.name)
                    except AttributeError:
                        self.job.post_data(
                            self.job.get_build_triggerurl()[0], "build"
                        )
                    return "{}{}{}{}rebuilding{}({}){}".format(
                        C["white"], self.print_job(), C["clear"],
                        C["green"], C[color], version_status, C["clear"]
                    )
                else:
                    return "{}{}{}{}{}:{}{}{}".format(
                        C["white"], self.print_job(), C["clear"],
                        C["green"], build_ver, C[color], version_status,
                        C["clear"]
                    )
            else:
                try:
                    self.jenkins_connection.build_job(self.job.name)
                except AttributeError:
                    self.job.post_data(
                        self.job.get_build_triggerurl()[0], "build"
                    )

                return "{}{}{}{}rebuilding{}({}){}".format(
                    C["white"], self.print_job(), C["clear"],
                    C["green"], C[color], version_status, C["clear"]
                )

    def get_job_status_by_version(
        self, build_ver, wiki, view_url, job_key=None, summary=False,
        last=False
    ):
        jenkins_helper_functions = JenkinsHelperFunctions(
            job_name=self.job.name, view_url=view_url
        )
        err = "N/A"

        version_status, build_num = (
            jenkins_helper_functions.get_version_status(build_ver, last=last)
        )
        if build_num is None:
            version_status = "N/A"
            return version_status

        job_run_url = "{}/job/{}/{}/testReport/".format(
            view_url, self.job.name, str(build_num.strip("\n"))
        )
        try:
            urllib2.urlopen(job_run_url)
        except urllib2.HTTPError:
            job_run_url = "{}/job/{}/{}/consoleText/".format(
                view_url, self.job.name, str(build_num.strip("\n"))
            )

        job_summery = JenkinsHelperFunctions(
            view_url=view_url, job_name=self.job.name).get_job_summery(
                job_run_url
            )

        if version_status == "Success":
            version_status += " "
            color = "green"
            status_sign = "{./}"
        elif version_status == "Unstable":
            if job_key:
                err = jenkins_helper_functions.get_fail_test(
                    job_run_url, job_key
                )
            color = "yellow"
            status_sign = "{*}"
        elif version_status == "Failed":
            if job_key:
                err = jenkins_helper_functions.get_fail_test(
                    job_run_url, job_key
                )
            version_status += "  "
            color = "red"
            status_sign = "{.x}"
        elif version_status == "Disabled":
            color = "dark_gray"
        else:
            version_status += "     "
            color = "white"

        reason = ("reason", C[color], err) if job_key else ""

        if not summary:
            if wiki:
                tests, fail = jenkins_helper_functions.get_job_result(
                    job_run_url
                )
                if tests is False:
                    return False
                total_fails = tests - fail
                tests_status = "{}/{}".format(str(tests), str(total_fails))
                return "||{}||{}[[{}|{}]]||{}||".format(
                    self.job.name, status_sign, job_run_url, tests_status, err
                )
            else:
                return "{}{}{}{}{} {}{}{}{}{} {}{}{} {}{} {}".format(
                    C["white"], self.print_job(), C["clear"], C[color],
                    version_status, C["clear"], C["white"], C["clear"],
                    C["green"], build_ver, C["clear"], C["white"], reason,
                    C["white"], job_summery, C["clear"]
                )
        return version_status

    def compare_status_by_version(self, build_ver, build_compare, view_url):
        jenkins_helper_functions = JenkinsHelperFunctions(
            job_name=self.job.name, view_url=view_url
        )

        version_status_1 = jenkins_helper_functions.get_version_status(
            build_ver)[0]
        version_status_2 = jenkins_helper_functions.get_version_status(
            build_compare)[0]
        if version_status_1 == "Success":
            version_status_1 += " "
            color = "green"
            v1_status = C[color] + version_status_1 + build_ver
        elif version_status_1 == "Unstable":
            color = "yellow"
            v1_status = C[color] + version_status_1 + build_ver
        elif version_status_1 == "Failed":
            version_status_1 += "  "
            color = "red"
            v1_status = C[color] + version_status_1 + build_ver
        elif version_status_1 == "Disabled":
            color = "dark_gray"
            v1_status = C[color] + version_status_1 + build_ver
        else:
            version_status_1 += "     "
            color = "white"
            v1_status = C[color] + version_status_1 + build_ver
        if version_status_2 == "Success":
            version_status_2 += " "
            color = "green"
            v2_status = C[color] + version_status_2 + build_compare
        elif version_status_2 == "Unstable":
            color = "yellow"
            v2_status = C[color] + version_status_2 + build_compare
        elif version_status_2 == "Failed":
            version_status_2 += "  "
            color = "red"
            v2_status = C[color] + version_status_2 + build_compare
        elif version_status_2 == "Disabled":
            color = "dark_gray"
            v2_status = C[color] + version_status_2 + build_compare
        else:
            version_status_2 += "     "
            color = "white"
            v2_status = C[color] + version_status_2 + build_compare

        return "{}{}{}{}{}{}{}".format(
            C["white"], self.print_job(), C["clear"],v1_status, C["clear"],
            v2_status, C["clear"]
        )

    def get_job_status(self):
        res = {}
        builds = [i for i in self.job.get_build_ids()]
        builds_objects = [self.job.get_build(i) for i in builds]
        statuses = [i.get_status() for i in builds_objects]
        builds_console = [i.get_console() for i in builds_objects]
        builds_version = [
            re.findall(r'RHEVM_BUILD=.*', i)[0].strip("RHEVM_BUILD=") for i in
            builds_console
        ]

        build_results_object = []
        for i in builds_objects:
            try:
                build_results_object.append(i.get_resultset())
            except NoResults:
                build_results_object.append(None)

        result_items = []
        for i in build_results_object:
            if i is not None:
                result_items.append(i.items())
            else:
                result_items.append(None)

        for a, b, c, in zip(builds, statuses, builds_version):
            res[a] = b, c

    def get_job_info(self, build_number):
        build_obj = self.job.get_build(int(build_number))
        build_status = build_obj.get_status()
        build_console = build_obj.get_console()
        build_version = re.findall(
            r'RHEVM_BUILD=.*', build_console)[0].strip("RHEVM_BUILD=")
        build_result_obj = build_obj.get_resultset()
        build_result = build_result_obj.items()
