#! /usr/bin/python
# -*- coding: utf-8 -*-

import click
import os
import user
import ConfigParser
from JenkinsHelper import (
    JenkinsConnection, C, JenkinsJobsAndView, JenkinsRunActions
)


class Params(object):
    def __init__(self, view=None, search=None, action=None, view_url=None):
        self.username = view
        self.password = search
        self.action = action
        self.view_url = view_url
        self.search = search


@click.group()
@click.pass_context
@click.option("--view", help="View in Jenkins,"
                             "Example: "
                             "--view Network/3.4-git",
              default="All")
@click.option("--search", help='''search for job.
                               <search1,search2> for 'and' search.
                               <search1:search2> for 'or' search''')
def jenkins_cli(ctx, view, action=None, view_url=None, search=None):
    """
    Run Jenkins tools from CLI

    \b
    conf file with server, user and password.
    File should be locate at $HOME/.jenkinsCLI.conf
    Example file:
    ################################
    # [SETTING]                    #
    # server=http://jenkins.server #
    # username=username            #
    # password=password            #
    ################################
    """
    ctx.obj = Params(view, search, action, view_url)
    ctx.obj.search = str(ctx.obj.search) if ctx.obj.search is not None else \
        None
    conf_file = user.home + "/.jenkinsCLI.conf"

    if not os.path.isfile(conf_file):
        print conf_file, "is missing"
        return False

    config = ConfigParser.RawConfigParser()
    config.read(conf_file)
    server = config.get("SETTING", "server")
    username = config.get("SETTING", "username")
    password = config.get("SETTING", "password")

    jenkins_obj = JenkinsConnection(server=server,
                                    username=username,
                                    password=password)

    jenkins_connection = jenkins_obj.jenkins_connection()

    jobs_and_view = JenkinsJobsAndView(
        jenkins_connection=jenkins_connection, view=view)

    ctx.obj.view_url = jobs_and_view.get_view()
    jobs_dict = jobs_and_view.get_jobs_dict()
    if not jobs_dict:
        return False

    ctx.obj.action = JenkinsRunActions(jobs_dict=jobs_dict,
                                       jenkins_connection=jenkins_connection)


@jenkins_cli.command()
@click.pass_obj
def list_jobs(ctx):
    """
    List all jobs under view
    """
    ctx.action.job_actions(action="print", view_url=ctx.view_url,
                           search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
@click.option("--wiki", help="Display output in wiki format", is_flag=True)
@click.option("--build_version", help="Build version to check status",
              required=True)
@click.option("--job_key", help="Key-word for fail cases")
@click.option("--summary", help="Get summary", is_flag=True)
@click.option("--last", help="Get status from last run", is_flag=True)
def get_ver_status(ctx, build_version, wiki=False,
                   job_key=None, summary=False, last=False):
    """
    Get best status for version.
    """
    ctx.action.job_actions(action="get_ver_status", view_url=ctx.view_url,
                           build_ver=build_version, wiki=wiki,
                           job_key=job_key, summary=summary, last=last,
                           search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
@click.option("--build_version", help="Base build version",
              required=True)
@click.option("--compare_version", help="Build version to compare to the "
                                        "base version", required=True)
def compare(ctx, build_version, compare_version):
    """
    Compare between to versions status
    """
    ctx.action.job_actions(action="compare", view_url=ctx.view_url,
                           build_ver=build_version,
                           build_compare=compare_version,
                           search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def enable(ctx):
    """
    Enable job/s
    """
    ctx.action.job_actions(action="enable", search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def disable(ctx):
    """
    Disable job/s
    """
    ctx.action.job_actions(action="disable", search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def build(ctx):
    """
    Build job/s
    """
    ctx.action.job_actions(action="build", view_url=ctx.view_url,
                           search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def delete(ctx):
    """
    Delete job/s
    """
    ctx.action.job_actions(action="delete", search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def is_queued(ctx):
    """
    Check if job/s is in the queued
    """
    ctx.action.job_actions(action="is_queued", search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def is_running(ctx):
    """
    Check if job/s is running
    """
    ctx.action.job_actions(action="is_running", search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def stop(ctx):
    """
    Stop running job/s
    """
    ctx.action.job_actions(action="stop", search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def kill(ctx):
    """
    Kill running and queued job/s
    """
    ctx.action.job_actions(action="kill", search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def rebuild(ctx):
    """
    Rebuild fail and unstable job/s
    """
    ctx.action.job_actions(action="rebuild", view_url=ctx.view_url,
                           search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
def delete_from_queue(ctx):
    """
    Delete job/s from queue
    """
    ctx.action.job_actions(action="delete_from_queue", search=ctx.search)


@jenkins_cli.command()
@click.pass_obj
@click.option("--build_version", help="Build version to check status",
              required=True)
@click.option("--job_key", help="Key-word for fail cases")
@click.option("--exclude", help="Exclude job/s from rebuild by failed reason")
@click.option("--status", help="Rebuild only job/s with this status")
def rebuild_by_version(ctx, build_version, exclude=None, job_key=None,
                       status=None):
    """
    Rebuild fail and unstable job/s by version
    """
    search = str(ctx.search) if ctx.search is not None else None
    ctx.action.job_actions(action="rebuild_by_ver",
                           build_ver=build_version, exclude=exclude,
                           job_key=job_key, status=status,
                           view_url=ctx.view_url, search=search)

if __name__ == '__main__':
    jenkins_cli()
